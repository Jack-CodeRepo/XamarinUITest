﻿using System;
using XamarinUITest.src.Base;

namespace XamarinUITest.src.Page
{

    internal class Page_01
    {
        private static Selector selector_01 = new Selector(x => x.Id("id_android"), x => x.Id("id_ios"));
        private static Selector selector_02 = new Selector(x => x.Id("btn_aprove"), x => x.Text("Yes"));
        private static Selector selector_03 = new Selector(x => x.Id("field_name"), x => x.Marked("field_user_input_name_because_long_id_are_fun_somehow_but_not_recommended"));

        public static void GoToMyElement_01()
        {
            Actions.ScrollDownTo(selector_01.Query);
        }


        public static void GoToPage_02()
        {
            Actions.ScrollDownTo(selector_02.Query);
            Actions.Tap(selector_02.Query);
        }


        public static void EnterText_03()
        {
            Actions.EnterText(selector_03.Query, "your text");
        }



    }
}

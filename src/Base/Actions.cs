﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;


namespace XamarinUITest.src.Base
{

    internal class Actions
    {
        private static IApp app => AppInitializer.App;

        protected static bool on_android => AppInitializer.Platform == Platform.Android;
        protected static bool on_ios => AppInitializer.Platform == Platform.iOS;

        public static void Tap(Selector selector)
        {
            ScrollDownTo(selector);
            app.Tap(selector.GetQuery());
        }

        public static void ScrollDownTo(Selector selector, int seconds=30)
        {
            app.ScrollDownTo(
                    selector.GetQuery(),
                    strategy: ScrollStrategy.Gesture,
                    swipePercentage: 0.8,
                    timeout: TimeSpan.FromSeconds(seconds)
                    );


        }

        public static void ScrollUpTo(Selector selector, int seconds=30)
        {
            app.ScrollUpTo(
                    selector.GetQuery(),
                    strategy: ScrollStrategy.Gesture,
                    swipePercentage: 0.8,
                    timeout: TimeSpan.FromSeconds(seconds)
                    );
        }


        public static void InvokeRepl()
        {
            app.Repl();
        }

        public static void EnterText(Selector selector, String text)
        {
            app.EnterText(selector.GetQuery(), text);
        }

        public static void WaitForElement(Selector selector, int seconds=5)
        {
            app.WaitForElement(
                selector.GetQuery(), 
                timeoutMessage: $"\n[ACTIONS] WaitForElement \n[QUERY] {selector.GetQueryAsString()} \n[END AS] Timeout with {seconds} seconds.",
                timeout: TimeSpan.FromSeconds(seconds)
                );
        }

    }
}

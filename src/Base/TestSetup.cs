﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;


namespace XamarinUITest.src.Base
{
    [TestFixture(Platform.Android)]
    [TestFixture(Platform.iOS)]

    public abstract class TestSetup
    {

        public abstract void BeforeTest();
        public abstract void BeforeEachTest();
        public abstract void AfterTest();
        public abstract void AfterEachTest();




        [OneTimeSetUp]
        public void _BeforeTest()
        {
            AppInitializer.StartApp();
            BeforeTest();
        }

        [SetUp]
        public void _BeforeEachTest()
        {
            BeforeEachTest();
        }

        [OneTimeTearDown]
        public void _AfterTest()
        {
            AppInitializer.StartApp();
            AfterTest();
        }

        [TearDown]
        public void _AfterEachTest()
        {
            AfterEachTest();
        }





    }
}

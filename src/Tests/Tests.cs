﻿using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

using XamarinUITest.src.Base;
using XamarinUITest.src.Page;

namespace XamarinUITest
{
    [TestFixture(Platform.Android)]
    [TestFixture(Platform.iOS)]
    public class Tests : TestSetup
    {
        IApp app;
        Platform platform;

        public Tests(Platform platform)
        {
            this.platform = platform;
        }

        public override void BeforeTest() { }
        public override void BeforeEachTest() { }
        public override void AfterTest() { }
        public override void AfterEachTest() { }








        [Test]
        public void FirstTest()
        {
            Page_01.EnterText_03();
            Page_01.GoToMyElement_01();
            Page_01.GoToPage_02();
        }
    }
}

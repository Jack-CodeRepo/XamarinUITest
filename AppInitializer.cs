﻿using System;
using Xamarin.UITest;



namespace XamarinUITest
{
    public class AppInitializer
    {

        private static string android_apk = "";     // path to local apk file - ie: c:\\TestProjects\\XamarinUITest\\Apps\\my_apk.apk
        private static string ios_ipa = "";         // name of install ipa on device

        private static IApp app;
        public static IApp App
        {
            get
            {
                if (app == null) { throw new NullReferenceException("AppInitializer failure. Startpp() method not called."); }
                return app;
            }
        }

        private static Platform? platform;
        public static Platform Platform
        {
            get { 
                if (platform == null) { throw new NullReferenceException("AppInitializer failure. Platform not set"); }
                return platform.Value;
            }
            set { platform = value; }
        }

        public static void StartApp()
        {
            if (platform == Platform.Android) 
            {
                app = ConfigureApp
                      .Android
                      .EnableLocalScreenshots()
                      .ApkFile(android_apk)
                      .StartApp(Xamarin.UITest.Configuration.AppDataMode.Clear);
            }

            if (platform == Platform.Android) 
            {
                app = ConfigureApp
                  .iOS
                  .EnableLocalScreenshots()
                  .InstalledApp(ios_ipa)
                  .DeviceIdentifier("your identifier")
                  .PreferIdeSettings()
                  .StartApp(Xamarin.UITest.Configuration.AppDataMode.Clear);
            }

        }




    }
}
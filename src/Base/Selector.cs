﻿using Xamarin.UITest.Queries;
using Xamarin.UITest;
using System;
using System.Linq.Expressions;


namespace XamarinUITest.src.Base
{
    using Query = Func<AppQuery, AppQuery>;
    internal class Selector
    {
        private bool on_android => AppInitializer.Platform == Platform.Android;
        private bool on_ios => AppInitializer.Platform == Platform.iOS;
        
        private Query query;
        private string query_str;


        public Selector(Expression<Query> ios_query, Expression<Query> android_query)
        {
            this. query = on_android ? android_query.Compile() : ios_query.Compile();
            this.query_str = on_android ? android_query.ToString() : ios_query.ToString();
        }
    
        public Query GetQuery()
        {
            return this.query;
        }

        public string GetQueryAsString()
        {
            return this.query_str;
        }










    }
}
